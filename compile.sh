#!/bin/bash

 # To run: source compile.sh
mkdir -p CMakeFiles
cd CMakeFiles
cmake  ../
make -j
cd ..

# This is needed to allow auto complete with FetchContent
path=$(pwd)
if [[ ":$PATH:" != *":$path"* ]]; then
  export PATH=$PATH:$path
fi
