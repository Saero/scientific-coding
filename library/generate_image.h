#ifndef GENERATE_IMAGE_H
#define GENERATE_IMAGE_H
#include <Eigen/Dense>
#include <mutex>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <atomic>

namespace generate_image {

namespace construct {

template<typename T>//!< This function creates a quick truth image that is a grayscale image that starts at 0 in the upper left and scales to 256 at the lower right
std::vector<std::vector<T>> truth(size_t rows, size_t cols){
  if(rows % 2){
    throw std::runtime_error("[generate_image][truth] rows variable is not divisible by 2");
  }
  if(cols % 2){
    throw std::runtime_error("[generate_image][truth] cols variable is not divisible by 2");
  }
  std::vector<std::vector<T>> mat(rows, std::vector<T>(cols, 0));
 
  float horizontal_slope{127.0f/cols};
  float vertical_slope{127.0f/rows};
  
  for(int row = 0; row < rows; ++row){
    for(int col = 0; col < cols; ++col){
      mat[row][col] = float(row) * vertical_slope + float(col) * horizontal_slope;
    }
  }
  return mat;
}


template<typename T>//!< This increases the size of a given "image", this is what is used for testing processing
std::vector<std::vector<T>> enhance(std::vector<std::vector<T>>& mat, size_t multiplier){
  if(multiplier % 2){
    throw std::runtime_error("[generate_image][eohance (2d vector)] multiplier needs to be a power of 2 ");
  }
  auto rows = mat.size();
  auto cols = mat[0].size(); 
  auto enhanced_rows = rows*multiplier;
  auto enhanced_cols = cols*multiplier;
  std::vector<std::vector<T>> enhanced_mat(enhanced_rows, std::vector<T>(enhanced_cols, 0));
  auto start_point{(float(multiplier * multiplier) / 2) - .5}; 

  for(int row = 0; row < enhanced_rows; ++row){
    for(int col = 0; col < enhanced_cols; ++col){
      // see enhance_eigen for details of below equation
      enhanced_mat[row][col] = mat[floor(float(row)/multiplier)][floor(float(col)/multiplier)]  
        + ((col%multiplier)) + (multiplier *  (row%multiplier)) 
        - start_point;
    }
  }
  return enhanced_mat;
}



template<typename T>//!< This function creates a quick truth image that is a grayscale image that starts at 0 in the upper left and scales to 256 at the lower right
T truth_eigen(size_t rows, size_t cols){
  if(rows % 2){
    throw std::runtime_error("[generate_image][truth_eigen] rows variable is not divisible by 2");
  }
  if(cols % 2){
    throw std::runtime_error("[generate_image][truoh_eigen] cols variable is not divisible by 2");
  }
  T mat(rows, cols);
 
  float horizontal_slope{127.0f/float(cols)};
  float vertical_slope{127.0f/float(rows)};
  
  for(int row = 0; row < rows; ++row){
    for(int col = 0; col < cols; ++col){
      mat(row, col) = float(row) * vertical_slope + float(col) * horizontal_slope;
    }
  }
  return mat;
}

template<typename T>//!< This increases the size of a given "image", this is what is used for testing processing
T enhance_eigen( T& mat, size_t multiplier){
  if(multiplier % 2){
    throw std::runtime_error("[generate_image][enhance (MatrixXd)] multiplier needs to be a power of 2 ");
  }
  auto rows = mat.rows();
  auto cols = mat.cols(); 
  auto enhanced_rows = rows*multiplier;
  auto enhanced_cols = cols*multiplier;
  T enhanced_mat(enhanced_rows, enhanced_cols);
  auto start_point{(float(multiplier * multiplier) / 2) - .5}; // combining two elements, the first being the start point of the line, the second is to account for the fact that the discrete set is an even size (0 will not be in the set)
  for(int row = 0; row < enhanced_rows; ++row){
    for(int col = 0; col < enhanced_cols; ++col){
      // the objective of this equation is to populate a grid of subelements 
      // numerically centered around the element, and when the average is taken
      // of the subelements, it equals the original element
      enhanced_mat(row, col) =  
        mat(int(floor(float(row)/multiplier)) , int(floor(float(col)/multiplier))) // get element that is to be enhanced
        + ((col%multiplier)) + (multiplier *  (row%multiplier)) // calculate slope
        - start_point;
    }
  }
  return enhanced_mat;
}

} // namespace generate

namespace arithmetic{

/**
* Takes in a large array of elements at a subelement resolution defined by 
* a multiplier and sums the sub elements to get the original element
*/
template<typename T>
void subelement_sum_mutex(
    std::vector<std::vector<T>>& input,
    std::vector<std::vector<T>>& output,
    size_t& elementX,
    size_t& elementY,
    const size_t multiplier, 
    const int thread_id,
    std::mutex& write_guard){

  size_t outputYSize{output.size()};
  size_t outputXSize{output[0].size()};
  size_t outputSize{ outputYSize * outputXSize };
  size_t indexY {0};
  size_t indexX {0};
  size_t XOffset{0};
  size_t XEnd   {0};
  size_t YOffset{0};
  size_t YEnd   {0};

  { // write_guard
    // TODO this assumes that the user gives the function different indicies
    std::lock_guard<std::mutex> lock(write_guard);
    indexX = elementX;
    indexY = elementY;
  } // write_guard

  T sum{};
  while(indexX*indexY < outputSize){

    XOffset = (multiplier * indexX);
    XEnd = XOffset + multiplier;
    YOffset = (multiplier * indexY);
    YEnd = YOffset + multiplier;

    // actual processing the summation
    for(size_t y = YOffset; y < YEnd; ++y){
      for(size_t x = XOffset; x < XEnd; ++x){
        sum += input[y][x];
      }
    }

    { // write_guard
      std::lock_guard<std::mutex> lock(write_guard);
      output[indexY][indexX] = double(sum) / (multiplier * multiplier);

      {
        // increment on to next element
        if(elementX < outputXSize-1){
          elementX = elementX + 1; 
        }
        else if(elementY < outputYSize-1){
          elementY = elementY + 1;
          elementX = 0;
        }
        else{
          return;// we hit the end of the output
        }
      }
      
      indexX = elementX;
      indexY = elementY;
    } // write_guard
    
    sum = 0;

  }
}



/**
* Takes in a large array of elements at a subelement resolution defined by 
* a multiplier and sums the  sub elements to get the original element
*/
template<typename T>
void subelement_sum_atomic(
  std::vector<std::vector<T>>& input,
  std::vector<std::vector<T>>& output,
  std::atomic_size_t& elementX,
  std::atomic_size_t& elementY,
  const size_t multiplier, 
  const int thread_id, 
  std::mutex& write_guard){

  size_t outputYSize{output.size()};
  size_t outputXSize{output[0].size()};
  size_t outputSize{ outputYSize * outputXSize };
  size_t indexY {0};
  size_t indexX {0};
  size_t XOffset{0};
  size_t XEnd   {0};
  size_t YOffset{0};
  size_t YEnd   {0};

  T sum{};
  while(elementX*elementY < outputSize){

    XOffset = (multiplier * indexX);
    XEnd = XOffset + multiplier;
    YOffset = (multiplier * indexY);
    YEnd = YOffset + multiplier;

    // actual processing the summation
    for(size_t y = YOffset; y < YEnd; ++y){
      for(size_t x = XOffset; x < XEnd; ++x){
        sum += input[y][x];
      }
    }
    {
      std::lock_guard<std::mutex> lock(write_guard);
      output[indexY][indexX] = double(sum) / (multiplier * multiplier);
    }
    sum = 0;

    {
      elementX.load(std::memory_order_acquire);
      elementY.load(std::memory_order_acquire);
      // increment on to next element
      if(elementX < outputXSize-1){

        indexX = elementX + 1;
        indexY = elementY;
        elementX.store( elementX + 1, std::memory_order_release);
        elementY.store( elementY, std::memory_order_release);
      }
      else if(elementY < outputYSize-1){

        indexX = 0;
        indexY = elementY + 1;
        elementX.store( 0, std::memory_order_release);
        elementY.store( elementY + 1, std::memory_order_release);
      }
      else{
        elementX.store( elementX, std::memory_order_release);
        elementY.store( elementY, std::memory_order_release);
        return;// we hit the end of the output
      }
    }

  }
}


template<typename T>
T subelement_sum_eigen(
    std::atomic_ref<T>& input
  , std::atomic_ref<T>& output
  , std::atomic<size_t> elementX
  , std::atomic<size_t> elementY
  , size_t multiplier){

  T sum{};
  size_t indexY {elementX};
  size_t indexX {elementY};
  size_t XOffset{(multiplier * indexY)};
  size_t XEnd   {XOffset + multiplier};
  size_t YOffset{(multiplier * indexY)};
  size_t YEnd   {YOffset + multiplier};

  for(size_t row = 0; row < YEnd; ++row){
    for(size_t col = 0; col < XEnd; ++col){
      sum += input(row, col);
    }
  }
  output(indexY, indexX) = double(sum) / (multiplier^2);
}

} // namespace arithmetic

namespace analysis{

template<typename T>
void compare(std::vector<std::vector<T>>& mat1, std::vector<std::vector<T>>& mat2){
  auto rowSize{mat1.size()};
  auto colSize{mat1[0].size()};
  auto rowSize2{mat1.size()};
  auto colSize2{mat1[0].size()};
  if(rowSize != rowSize2){
    throw std::runtime_error("[generate_iamge][analysis] row sizes do not match");
  }
  if(colSize != colSize2){
    throw std::runtime_error("[generate_iamge][analysis] column sizes do not matoh");
  }
  bool succeeded{true};
  for(size_t row{0}; row < rowSize; ++row){
    for(size_t col{0}; col < colSize; ++col){
      if(mat1[row][col] != mat2[row][col]){
        std::cout<<"NOPE!"<<" row: "<<row<<" col: "<<col<<" value from truth: "<< mat1[row][col]<<" value from results: "<<mat2[row][col]<<std::endl;
        succeeded = false;
      }
    }
  }
  if(succeeded){
    std::cout<<"All outputs match truth"<<std::endl;
  }
}

template<typename T>
void compare_eigen(T& mat1, T& mat2){
 ;
}

} // namespace analysis
} // namespace image


#endif
