
#include "library/generate_image.h"

#include <vector>
#include <Eigen/Dense>
#include <matplot/matplot.h>
#include <thread>

namespace{

std::vector<std::vector<double>> eigen2std(Eigen::MatrixXd eigen){
  int rows = eigen.rows();
  int cols = eigen.cols();
  std::vector<std::vector<double>> mat_eigen_std(rows, std::vector<double>(cols,0));
  for(int row=0; row < rows; ++row){
    for(int col=0; col < cols; ++col){
      mat_eigen_std[row][col] = eigen(row, col);
    }
  }
  return mat_eigen_std;
}
} // namespace

void printMat(std::vector<std::vector<double>> mat){
  for(auto& row : mat){
    for(auto& col : row){
      std::cout<<col<<" ";
    }
    std::cout<<'\n';
  }
}


int main(){

  // configuration
  int rows = 400;
  int cols = 400;
  int multiplier = 4; // the multiplier is what 
  int threadPool = 13;


  auto mat_eigen{generate_image::construct::truth_eigen<Eigen::MatrixXd>(rows, cols)};
  auto mat_enhance_eigen{generate_image::construct::enhance_eigen<Eigen::MatrixXd>(mat_eigen, multiplier)};
  auto mat_eigen_std{eigen2std(mat_eigen)};
  auto mat_eigen_enhanced_std{eigen2std(mat_enhance_eigen)};
  

  //matplot::image(mat_eigen_std);
  //matplot::show();
  //matplot::image(mat_eigen_enhanced_std);
  //matplot::show();


  // Set up standard containers, try atomics and mutexes
  typedef std::vector<std::vector<double>> nestedV;
  auto mat{generate_image::construct::truth<double>(rows,cols)};
  auto mat_high_res{generate_image::construct::enhance(mat, multiplier)};
  std::vector<std::vector<double>> mat_zeros(rows, std::vector<double>(cols, 0));


  //image::arithmetic::subelement_sum_mutex test
  std::vector<std::thread> threads;
  std::mutex write_guard;
  size_t elementX{0};
  size_t elementY{0};
  for(size_t threadCount{0}; threadCount < threadPool; ++threadCount){
    elementX = threadCount;
    threads.emplace_back(std::thread(generate_image::arithmetic::subelement_sum_mutex<double>, 
                                     std::ref(mat_high_res), 
                                     std::ref(mat_zeros), 
                                     std::ref(elementX),
                                     std::ref(elementY),
                                     multiplier, 
                                     threadCount, 
                                     std::ref(write_guard)));
  }
  for(auto& thread: threads){
    thread.join();
  }
  generate_image::analysis::compare( mat, mat_zeros );


  //image::arithmetic::subelement_sum_atomic test
  std::vector<std::thread> threads2;
  std::atomic_size_t elementX_a{0};
  std::atomic_size_t elementY_a{0};
  std::mutex write_guard2;
  for(size_t threadCount{0}; threadCount < threadPool; ++threadCount){
    elementX_a = threadCount;
    threads2.emplace_back(std::thread(generate_image::arithmetic::subelement_sum_atomic<double>, 
                                     std::ref(mat_high_res), 
                                     std::ref(mat_zeros), 
                                     std::ref(elementX_a),
                                     std::ref(elementY_a),
                                     multiplier, 
                                     threadCount, 
                                     std::ref(write_guard2)));
  }
  for(auto& thread: threads2){
    thread.join();
  }
  generate_image::analysis::compare( mat, mat_zeros );



  //
  //matplot::image(mat);
  //matplot::show();
  //matplot::image(mat_high_res);
  //matplot::show();

  return 0;
}
